#include <stdio.h>
#define MAX 50
int main()
{
  int arows, acolumns, brows, bcolumns, i, j, k, sum = 0;
  int a[MAX][MAX], b[MAX][MAX], multiply[MAX][MAX],addition[MAX][MAX];
 
  printf("Enter number of rows and columns of first matrix\n");
  scanf("%d%d", &arows, &acolumns);
  
  
  	 printf("Enter elements of first matrix\n");
 
  for (i = 0; i < arows; i++)
    for (j = 0; j <acolumns ; j++)
      scanf("%d", &a[i][j]);
      
      printf("Enter number of rows and columns of second matrix\n");
  scanf("%d%d", &brows, &bcolumns);
 
 //mulplication of matrices of a matrix columns must be equal to b matrix rows
  if (acolumns != brows)
    printf("The multiplication isn't possible.\n");
  else
  { 
       printf("Enter elements of second matrix\n");
 
    for (i = 0; i< brows; i++)
      for (j = 0; j < bcolumns; j++)
        scanf("%d", &b[i][j]);
 
 //multiplication
    for (i = 0; i < arows; i++) {
      for (j = 0; j < bcolumns; j++) {
        for (k = 0; k < brows; k++) {
          sum = sum + a[i][k]*b[k][j];
        }
 
        multiply[i][j] = sum;
        sum = 0;
      }
    }
 //multiplication printing result
    printf("multiplication of the matrices:\n");
 
    for (i = 0; i< arows; i++) {
      for (j = 0; j < bcolumns; j++)
        printf("%d\t", multiply[i][j]);
 
      printf("\n");
    }
  }
  //addition
  for(i=0;i<arows;i++){
	 	for(j=0;j<bcolumns;j++){
	 	addition[i][j]=a[i][j]+b[i][j];
		 }
		 
	 }
	 //printing the addition
	 printf("Addition of matrices:\n");
	 for(i=0;i<arows;i++){
	 	for(j=0;j<bcolumns;j++){
	 		printf("%d\t",addition[i][j]);
	
		 }
		 printf("\n");
	 }
 
  return 0;
}
